#! /usr/bin/python2.7

import sys

inputfile = open(sys.argv[1], 'r')
f = open('output', 'w')
inputlines = inputfile.read().split('\n')
case_number = int(inputlines[0])

cases = []

def weight(value):
    arr = [0,1,2,3,3,4,4,5,5,5]
    return arr[value]

for i in range(0, case_number):
    cases.append((int(inputlines[2*i + 1]),
        [int(x) for x in inputlines[2*i + 2].split(' ')]))
    # cases contains an array of tuples : (int : D, int[] : Pancakes)

iterator = 0
for case in cases:
    iterator += 1
    minutes = 0
    numb_max = case[1].count(max(case[1]))
    max_i = case[1].index(max(case[1]))
    d = case[0]
    while case[1][max_i] > 1:
        if numb_max == 1 or case[1][max_i] > numb_max + ((case[1][max_i] + 1) / 2):
            print numb_max
            new = case[1][max_i] / 2
            case[1].append(new)
            case[1][max_i] -= new
            minutes += 1
        else:
            for i in range(0, len(case[1])):
                if case[1][i] > 0:
                    case[1][i] -= 1
            minutes += 1
        max_i = case[1].index(max(case[1]))
        numb_max = case[1].count(max(case[1]))
    f.write("Case #" + str(iterator) + ": " + str(minutes + 1) + '\n')
