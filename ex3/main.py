#! /usr/bin/python2.7

def get_index(val):
    a = ['1', 'i', 'j', 'k']
    return a.index(val)

def multiply(x, y):
    a = [['1', 'i', 'j', 'k'],
         ['i', '-1', 'k', '-j'],
         ['j', '-k', '-1', 'i'],
         ['k', 'j', '-i', '-1']]
    return a[get_index(x)][get_index(y)]

str = "ijkikjikij"

while
